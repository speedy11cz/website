$(document).ready(function(){       

   var scrollStart = 0;

   var startChange = $("#body");

   var offset = startChange.offset();

   $(document).scroll(function() { 

      scrollStart = $(this).scrollTop();

      if(scrollStart > offset.top) {

          $("#navigation").css("color", "#191919");

          $("#navigation").css("background", "#fff");

          $("ul#navigation li a").css("color", "#191919");

       } else {

          $("#navigation").css("color", "#fff");

          $("ul#navigation li a").css("color", "#fff");

          $("#navigation").css("background", "transparent");

       }

   });

});



var strings = ["vývojář^1000", "speedy^1000", "blázen^1000", "hráč^1000", "streamer^1000"];

$(function(){

    $(".animated").typed({

        strings: strings,

        typeSpeed: 50,

        loop: true,

        loopCount: Infinity,

        shuffle: true

    });

});



function toggleMenu() {

    var x = document.getElementById("navigation");

    if (x.className === "shown") {

        x.className = "hidden";

    } else {

        x.className = "shown";

    }

}



window.setInterval(function(){

    var d2 = new Date();

    var d1 = new Date(2004, 9, 11);

    var diff = d2.getTime() - d1.getTime();

    var number = diff / (1000 * 60 * 60 * 24 * 365.25)

    $(".current-age").html(number.toFixed(10));

}, 100);



$(function () {

  $("[data-toggle=\"tooltip\"]").tooltip();

});